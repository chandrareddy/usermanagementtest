# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
users = User.create([{first_name: 'Ajay', last_name: 'Siduu', email: 'ajay@vsc.com', password: "123456789123"}, 
	                 {first_name: 'Krishna', last_name: 'KM', email: 'krishna@vsc.com', password: "123456789123"},
	                 {first_name: 'Mohan', last_name: 'Reddy', email: 'mohan@vsc.com', password: "123456789123"},
	                 {first_name: 'Kovi', last_name: 'BB', email: 'kovi@vsc.com', password: "123456789123"}
	                ])