class UserMailer < ActionMailer::Base
  
  default from: "no-reply@usermanagement.com"

  def send_email_users(email, subject, body)
    @email = email
    @subject = subject
    @body = body
    mail(to: @email, subject: @subject)
  end
end
