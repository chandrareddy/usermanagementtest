class UsersController < ApplicationController
  before_action :authenticate_user!

  def index
    @users = User.all.where("id != ?", current_user.id).order('created_at DESC')    
  end

  def show
    @user = User.find(params[:id])
  end
 
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to users_path, :alert => "User Successfully Deleted."
  end

  def compose
    @users = User.all.where("id != ?", current_user.id).order('created_at DESC')
  end

  def compose_email
    @user_emails = params[:useremail]
    @user_emails.each do |email|
      UserMailer.send_email_users(email, params[:subject], params[:body]).deliver_now
    end
    redirect_to users_path, :notice => "User Emails Successfully Delivered."
  end
end
