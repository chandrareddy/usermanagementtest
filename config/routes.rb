Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout'}
  devise_for :users, controllers: { registrations: "registrations" }
  
  devise_scope :user do
    root to: "devise/sessions#new"
  end
  
  resources :users do
  	collection do
  	  get 'compose'
      get 'compose_email'
    end 
  end
end
